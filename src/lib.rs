// Because it is a lib, we do not care about dead code,
// if enabled, it could pollute the compilation output.
#![allow(dead_code)]
#![feature(const_generics)]
#![feature(const_evaluatable_checked)]

extern crate generic_array;
extern crate num;

mod matrix_base;

pub mod matrix;
pub mod vector;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn vector3f_add() {
        let vec1 = vector::Vector2f::new(4.0, 1.0);
        let nvec1 = vector::Vector2f::from_one(2.0);
        let vec2 = vec1 + nvec1;

        assert_eq!(vec2.x(), vec1.x() + nvec1.x());
        assert_eq!(vec2.y(), vec1.y() + nvec1.y());
    }

    #[test]
    fn vector3f_minus() {
        let vec1 = vector::Vector2f::new(4.0, 1.0);
        let nvec1 = vector::Vector2f::from_one(2.0);
        let vec2 = vec1 - nvec1;

        assert_eq!(vec2.x(), vec1.x() - nvec1.x());
        assert_eq!(vec2.y(), vec1.y() - nvec1.y());
    }

    #[test]
    fn vector3f_dot() {
        let vec1 = vector::Vector3f::new(1.0, 0.0, 0.0);
        let vec2 = vector::Vector3f::new(0.0, 1.0, 0.0);

        let dot_res = vec1.dot(vec2);

        assert_eq!(dot_res, 0.0);
    }

    #[test]
    fn vector3_cross() {
        let vec1 = vector::Vector3f::new(2.0, 3.0, 4.0);
        let vec2 = vector::Vector3f::new(5.0, 6.0, 7.0);

        let cross_res = vec1.cross(vec2);

        assert_eq!(cross_res.x(), -3.0);
        assert_eq!(cross_res.y(), 6.0);
        assert_eq!(cross_res.z(), -3.0);
    }

    #[test]
    fn matrix_mul() {
        let vec1 = vector::Vector3f::from_one(0.0);
        let vec2 = vector::Vector3f::from_one(0.0).transpose();

        let res = vec1 * vec2;

        assert_eq!(res.cols(), 3);
        assert_eq!(res.rows(), 3);
    }

    #[test]
    fn matrix_rows() {
        let vec1 = vector::Vector3f::from_one(0.0);

        let rows = vec1.rows();
        assert_eq!(rows, 3);
    }

    #[test]
    fn matrix_cols() {
        let vec1 = vector::Vector3f::from_one(0.0).transpose();

        let cols = vec1.cols();
        assert_eq!(cols, 3);
    }

    #[test]
    fn matrix_scaler_add() {
        let vec1 = vector::Vector3f::from_one(0.0);

        let vec2 = vec1 + 2.0;

        assert_eq!(vec2.x(), 2.0);
        assert_eq!(vec2.y(), 2.0);
        assert_eq!(vec2.z(), 2.0);
    }

    #[test]
    fn matrix_scalar_mul() {
        let vec1 = vector::Vector3f::from_one(1.0);

        let vec2 = vec1 * 2.0;

        assert_eq!(vec2.x(), 2.0);
        assert_eq!(vec2.y(), 2.0);
        assert_eq!(vec2.z(), 2.0);
    }

    #[test]
    fn matrix_scaler_sub() {
        let vec1 = vector::Vector3f::from_one(0.0);

        let vec2 = vec1 - 2.0;

        assert_eq!(vec2.x(), -2.0);
        assert_eq!(vec2.y(), -2.0);
        assert_eq!(vec2.z(), -2.0);
    }

    #[test]
    fn matrix_scalar_div() {
        let vec1 = vector::Vector3f::from_one(1.0);

        let vec2 = vec1 / 2.0;

        assert_eq!(vec2.x(), 0.5);
        assert_eq!(vec2.y(), 0.5);
        assert_eq!(vec2.z(), 0.5);
    }

    #[test]
    fn matrix_identity() {
        let mat = matrix::Matrix4f::identity();

        assert_eq!(mat.at(0, 0), 1.0);
        assert_eq!(mat.at(1, 1), 1.0);
        assert_eq!(mat.at(2, 2), 1.0);
        assert_eq!(mat.at(3, 3), 1.0);
    }
}
