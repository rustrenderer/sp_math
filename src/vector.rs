use crate::matrix_base::MatrixBase;

use generic_array::typenum as typenum;

use typenum::{U1, U2, U3, U4, Unsigned};
use generic_array::{ArrayLength, arr};
use num::{Num, FromPrimitive, ToPrimitive};
use std::ops;

pub type Vector<T, N> = MatrixBase<T, N, U1>;

impl <T, N> Vector<T,N>
where
    T: Num + FromPrimitive + ToPrimitive + ops::Mul<T> + ops::Add<T> + Copy + Clone,
    N: ArrayLength<T> + Copy + Clone,
    typenum::UInt<typenum::UTerm, typenum::B1>: ops::Mul<N>,
    <typenum::UInt<typenum::UTerm, typenum::B1> as ops::Mul<N>>::Output: ArrayLength<T> + Copy + Clone,
    <<typenum::UInt<typenum::UTerm, typenum::B1> as ops::Mul<N>>::Output as ArrayLength<T>>::ArrayType: Copy + Clone,
{
    pub fn dot (self, other: Self) -> T {
        let comp: usize = <N as Unsigned>::to_u32() as usize;

        let mut result = T::from_u32(0)
            .expect("T::from_u32(0 failed.");

        for i in 0..comp 
        {
            result = result + (self.data[i] * other.data[i]);
        }

        result
    }

    pub fn norm (self) -> f32 {
       self.squared_norm().sqrt()
    }

    pub fn squared_norm (self) -> f32 {
        let mut sum = 0.0;
        let comp = <N as Unsigned>::to_u32() as usize ;
        
        for i in 0..comp {
            let value : f32 = self.data[i].to_f32()
                .expect("data[_].to_f32() failed.");
            sum = sum + (value * value);
        }

        sum
    }
}

////////////////////////////////////
pub type Vector4<T> = Vector<T, U4>;

impl <T> Vector4<T> 
where
    T: Num + Copy + Clone
{
    pub fn new (x: T, y: T, z: T, w: T) -> Self {
        Vector4 { data: arr![T; x,y,z,w] }
    }

    pub fn from_one (x: T) -> Self {
        Vector4::<T>::new(x, x, x, x)
    }

    pub fn from_vector3 (vector: Vector3<T>, w: T) -> Self {
        Vector4::<T>::new(vector.x(), vector.y(), vector.z(), w)
    }
}

pub type Vector4f = Vector4<f32>;
pub type Vector4i = Vector4<i32>;
pub type Vector4u = Vector4<u32>;

////////////////////////////////////
pub type Vector3<T> = Vector<T, U3>;

impl <T> Vector3<T> 
where 
    T: Num + ops::Add<T> + ops::Mul<T> + Copy + Clone
{
    pub fn new (x: T, y: T, z: T) -> Self {
        Vector3 { data: arr![T; x,y,z ] }
    }

    pub fn from_one (x: T) -> Self {
        Vector3::<T>::new(x, x, x)
    }

    pub fn from_vector2 (vector: Vector2<T>, z: T) -> Self {
        Vector3::<T>::new(vector.x(), vector.y(), z)
    }

    pub fn cross(self, other: Self) -> Self {
        Vector3::<T>::new(
            (self.data[1] * other.data[2]) - (self.data[2] * other.data[1]),
            (self.data[2] * other.data[0]) - (self.data[0] * other.data[2]),
            (self.data[0] * other.data[1]) - (self.data[1] * other.data[0]))
    }

    pub fn x(self) -> T {
        self.data[0]
    }

    pub fn y(self) -> T {
        self.data[1]
    }

    pub fn z(self) -> T {
        self.data[2]
    }

    pub fn xy (self) -> Vector2<T> { 
        Vector2::<T>::new(self.data[0], self.data[1])
    }

    pub fn xz (self) -> Vector2<T> { 
        Vector2::<T>::new(self.data[0], self.data[2])
    }

    pub fn yx (self) -> Vector2<T> { 
        Vector2::<T>::new(self.data[1], self.data[0])
    }

    pub fn yz (self) -> Vector2<T> { 
        Vector2::<T>::new(self.data[1], self.data[2])
    }

    pub fn zx (self) -> Vector2<T> { 
        Vector2::<T>::new(self.data[2], self.data[0])
    }

    pub fn zy (self) -> Vector2<T> { 
        Vector2::<T>::new(self.data[2], self.data[1])
    }

    pub fn xzy (self) -> Vector3<T> { 
        Vector3::<T>::new(self.data[0], self.data[2], self.data[1])
    }

    pub fn yxz (self) -> Vector3<T> { 
        Vector3::<T>::new(self.data[1], self.data[0], self.data[2])
    }

    pub fn yzx (self) -> Vector3<T> { 
        Vector3::<T>::new(self.data[1], self.data[2], self.data[0])
    }
    
    pub fn zxy (self) -> Vector3<T> { 
        Vector3::<T>::new(self.data[2], self.data[0], self.data[1])
    }

    pub fn zyx (self) -> Vector3<T> { 
        Vector3::<T>::new(self.data[2], self.data[1], self.data[0])
    }
}

pub type Vector3f = Vector3<f32>;
pub type Vector3i = Vector3<i32>;
pub type Vector3u = Vector3<u32>;
pub type Vector3c = Vector3<u8>;

impl Vector3f {
    pub fn normalized(self) -> Self {
        self * (1.0 / self.norm())
    }
}

////////////////////////////////////
pub type Vector2<T> = Vector<T, U2>;

impl <T> Vector2<T>
where
    T: Num + Copy + Clone
{
    pub fn new (x: T, y: T) -> Self {
        Vector2 { data: arr![T; x, y] }
    }

    pub fn from_one (x: T) -> Self {
        Vector2::<T>::new(x, x)
    }

    pub fn x(self) -> T {
        self.data[0]
    }

    pub fn y(self) -> T {
        self.data[1]
    }

    pub fn yx (self) -> Self {
        Vector2::<T>::new (self.data[1], self.data[0])
    }
}

pub type Vector2f = Vector2<f32>;
pub type Vector2i = Vector2<i32>;
pub type Vector2u = Vector2<u32>;
